#! /usr/bin/env bash

if [ -z "$1" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    echo "Usage:  ./search_lectures.sh <keyword>"
    echo "Please run with --setup the first time to be able to search in PDFs"
    echo "Please set \$COMPSYSPUB to your local compSys-e2017-pub directory"
    echo "NOTE: This script searches only on the first argument given and that's an extended grep regex string."
    echo ""
    echo "Example: COMPSYSPUB=\"../compSys-e2017-pub/\" ./search_lectures.sh test"
    exit 0
fi

if [ -z "$COMPSYSPUB" ]; then
    # Default to my system path, because fuck you (nothing personel kid)
    COMPSYSPUB="$HOME/Dropbox/diku/compsys/compSys-e2017-pub/"
fi

if [ "$1" == "--setup" ]; then
    echo "Extracting text from pdfs"
    find "$COMPSYSPUB"/lectures/ -name "*.pdf" | xargs -L1 pdftotext
    exit 0
fi

find "$COMPSYSPUB"/lectures/ | grep "txt\|md" | xargs grep -i -e "$1"

